<?php

namespace ReliableOffshore\CodeceptionChatApps\Modules;

use AcceptanceTester;
use Codeception\Module\WebDriver;
use Codeception\Scenario;
use Illuminate\Support\Arr;

trait BaseAcceptanceTrait
{
    /**
     * @var AcceptanceTester|WebDriver|\Codeception\Module\Asserts|\ReliableOffshore\CodeceptionApps\Modules\WebDriverModule|\ReliableOffshore\CodeceptionApps\Modules\TestRailModule
     */
    protected $i;

    /**
     * @var \Codeception\Scenario
     */
    protected $scenario;

    public function _before(AcceptanceTester $i, Scenario $scenario)
    {
        $this->i = $i;
        $this->scenario = $scenario;
    }

    protected function getScenario()
    {
        return $this->scenario;
    }

    protected function wantToDisplayCartesianResults(array $output)
    {
        $test = "\n\n";
        $error = false;
        $colorCodeResult = function($result) use(&$error)
        {
            if ($result == 'fail')
                $error = true;

            return $result;
        };

        // @todo table layout..
        foreach ($output as $scenario => $scenarios)
        {
            $description = Arr::get($scenarios, 'info.description', $scenario);

            $result = Arr::get($scenarios, 'info.result');
            if ($result)
                $description .= ":\t\t " . $colorCodeResult($result);
            $test .= "$description\n";
            $caseInfo = Arr::get($scenarios, 'cases.info');
            $cases = (array) Arr::get($scenarios, 'cases.results');

            if (count($cases))
            {
                foreach ($cases as $case => $result)
                {
                    $test .= "\t$case $caseInfo :\t  $result\n";
                }
                $test .= "\n\n";
            }
        }

        $this->i->wantTo($test);

        if ($error)
            throw new \Exception('Some tests failed..');
    }
}