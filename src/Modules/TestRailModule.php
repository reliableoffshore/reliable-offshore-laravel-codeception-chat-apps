<?php

namespace ReliableOffshore\CodeceptionChatApps\Modules;

use Codeception\Actor;
use Codeception\Lib\ModuleContainer;
use Codeception\Module;
use Codeception\Step;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class TestRailModule extends Module
{
    use BaseModuleTrait;

    /**
     * @var TestRailAPIClient
     */
    protected static $api;
    protected static $testRunId;
    protected $url;
    protected $user;
    protected $password;
    protected $projectId;
    protected $auto_report;
    protected $auto_report_close;

    public function __construct(ModuleContainer $moduleContainer, $config = null)
    {
        parent::__construct($moduleContainer, $config);

        $this->url = Arr::get($config, 'url');
        $this->user = Arr::get($config, 'user');
        $this->password = Arr::get($config, 'password');
        $this->projectId = Arr::get($config, 'project');
        $this->auto_report = Arr::get($config, 'auto_report');
        $this->auto_report_close = Arr::get($config, 'auto_report_close');
    }

    public function _beforeSuite($settings = array())
    {
        $api = new TestRailAPIClient($this->url);
        self::$api = $api;
        $api->set_user($this->user);
        $api->set_password($this->password);

        if (!defined("STDIN"))
            define("STDIN", fopen('php://stdin', 'rb'));

        $name = 'Automation by '. get_current_user() .' @ ' . date('y/m/d h:i a');
        /* Define STDIN in case if it is not already defined by PHP for some reason */
        if (!$this->auto_report)
        {
            echo 'Enter TestRail test id or create a new test run? [$id|y|n] (default: n): ';
            $input = trim(fread(STDIN, 80)); // Read up to 80 characters or a newline

            if ($input == 'y')
            {
                self::$testRunId = Arr::get($api->send_post('add_run/' . $this->projectId, ['name' => $name, 'include_all' => true]), 'id');

                // ask for auto closing
                echo 'Would you like to auto close report afterwards? [y|n] (default: y): ';
                $input = trim(fread(STDIN, 80)); // Read up to 80 characters or a newline

                if ($input == 'y' || !$input)
                    $this->auto_report_close = true;
            }
            else if ($input != 'n' && ($input = intval($input)))
                self::$testRunId = $input;
        }
        // auto create
        else
        {
            self::$testRunId = Arr::get($api->send_post('add_run/' . $this->projectId, ['name' => $name, 'include_all' => true]), 'id');
        }

        if (self::$testRunId)
            echo "TestRail test run url: {$this->url}/index.php?/runs/view/" . self::$testRunId . "\n";
    }

    public function _afterSuite()
    {
        if ($this->auto_report_close)
        {
            try
            {
                self::$api->send_post('close_run/' . self::$testRunId, []);
            }
            catch (\Exception $e)
            {
                $this->debug('TestRail failed to close run: ' . $e->getMessage());
            }
        }
    }

    public function getTestCaseId($class, $method)
    {
        $reflection = new \ReflectionClass($class);
        $comments = $reflection->getMethod($method)->getDocComment();
        preg_match('/@tr-id\s+(\w+)/', $comments, $matches);

        return array_pop($matches);
    }

    /**
     * @param Actor|WebDriverModule $i
     * @param string|array $mixed
     * @param string $status
     * @param string|null $comment
     */
    public function testCaseResult(Actor $i, $mixed, $status, $comment = null)
    {
        $resultId = null;
        $statuses = [
            'pass' => 1,
            'fail' => 5
        ];

        if (is_array($mixed))
        {
            $class = $mixed[0];
            $method = $mixed[1];
            $caseId = $this->getTestCaseId($class, $method);
        }
        else
            $caseId = $mixed;

        try {
            $testRunId = self::$testRunId;

            if ($testRunId)
            {
                $comment = substr($comment, 0, 1500);
                $result = self::$api->send_post("add_result_for_case/$testRunId/$caseId",
                                                ['status_id' => Arr::get($statuses, $status),
                                                 'comment' => $comment]);
                $resultId = Arr::get($result, 'id');
            }
        } catch (\Exception $e)
        {
            $this->debug('TestRail failed to add result: '. $e->getMessage());
        }

        try
        {
            $info = self::$api->send_get("get_case/$caseId");
            $title = Str::slug(Arr::get($info, 'title'));
            $fileName = codecept_output_dir("run_{$testRunId}_") . "{$caseId}_{$status}-{$title}.png";
            $i->saveScreenshot($fileName);

            // attach result
            if ($resultId) {
                self::$api->send_post("add_attachment_to_result/$resultId", $fileName);
            }
        } catch (\Exception $e)
        {
            $this->debug('TestRail failed to store image : '. $e->getMessage());
        }

        $i->pause();
    }
}
