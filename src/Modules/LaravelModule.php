<?php

namespace ReliableOffshore\CodeceptionChatApps\Modules;

use Codeception\Lib\ModuleContainer;
use Codeception\Module\Laravel5 as BaseLaravel5;
use Illuminate\Support\Arr;

class LaravelModule extends BaseLaravel5
{
    use BaseModuleTrait;

    /**
     * @var \Codeception\Lib\ModuleContainer
     */
    protected $container;

    public function __construct(ModuleContainer $container, $config = NULL)
    {
        $this->container = $container;
        parent::__construct($container, $config);
    }
}
