<?php

namespace ReliableOffshore\CodeceptionChatApps\Modules;

use Codeception\Scenario;
use Codeception\Step;
use Codeception\Test\Cest;
use Illuminate\Support\Arr;

trait BaseModuleTrait
{
    /**
     * @var Step
     */
    protected $lastStep = null;

    public function _beforeStep(Step $step)
    {
        $this->lastStep = $step;
    }

    public function getLastStepAction()
    {
        if ($this->lastStep)
        {
            var_dump([
                $this->lastStep->getAction(),
                $this->lastStep->getArguments(),
                $this->lastStep->getLine(),
                $this->lastStep->getName(),
                $this->lastStep->getHumanizedActionWithoutArguments(),
                $this->lastStep->getHumanizedArguments(),
                $this->lastStep->getPhpCode(120)
            ]);
        }
    }

}
