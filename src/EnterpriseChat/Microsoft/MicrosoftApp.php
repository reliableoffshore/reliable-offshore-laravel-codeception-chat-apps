<?php

namespace ReliableOffshore\CodeceptionChatApps\EnterpriseChat\Microsoft;

use Exception;
use Codeception\Actor;
use Codeception\Util\Locator;
use Facebook\WebDriver\WebDriverKeys;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use ReliableOffshore\CodeceptionChatApps\EnterpriseChat\PlatformApp;

class MicrosoftApp extends PlatformApp
{
    public $selectorMessageInput = 'div.ts-editor-section div.ts-edit-box';
    public $selectorMessagesList = 'div.ts-message-list-container div.ts-message-list-item div.clearfix';
    public $selectorTeam = 'ul.ts-tree-group li.match-parent ul.channel-list-channels li a.channel-anchor';
    public $selectorMessageMoreMenu = 'div.message-actions button';
    public $selectorMessageMenuEdit = 'div#messageActionDropdown button[data-tid="messageEditButton"]';
    public $selectorMessageMenuDelete = 'div#messageActionDropdown button[data-tid="messageDeleteButton"]';
    public $selectorMessageEditInput = 'div.ts-edit-box';

    /**
     * *************************************
     *             Auth
     * *************************************
     */
    public function install($url = null)
    {
        $this->login('install');
    }

    public function login($path = null)
    {
        if (!$path)
            $this->i->amOnUrl($this->workspaceUrl());

        // option to pick account?
        try {
            $this->i->waitForElement('div#otherTile', 7);
            $this->i->click('div#otherTile');
        } catch (\Exception $exception) {}


        $usernameElm = 'input[name="loginfmt"]';
        $this->i->waitForElement($usernameElm, $this->waitForElements);
        $this->i->fillField($usernameElm, $this->username);
        $this->i->waitForElement('input[type="submit"]');
        $this->i->click('input[type="submit"]');
        $pwdElm = 'input[name="passwd"]';
        $this->i->waitForElement($pwdElm, $this->waitForElements);
        $this->i->fillField($pwdElm, $this->password);
        $this->i->wait(2);
        $submitElm ='input[value="Sign in"]';
        $this->i->findElement($submitElm);
        $this->i->click($submitElm);

        if ($path == 'install')
        {
            // see the accept page?
            try
            {
                $this->i->waitForText('Permissions requested', 7);
                $this->i->click('input[type="submit"]');
            }
            catch (\Exception $exception)
            {
            }
        }

        /** Next is button to stay login or not */
        try
        {
            $this->i->waitForText('Stay signed in', 5);
            $this->i->click('input[type="submit"]');
        }catch (\Exception $exception) {}

        if ($path != 'install') {
            $this->waitForAppToLoad();
        }
    }

    public function logout()
    {
        $this->i->click('div.profile-img-parent img.user-picture');
        $buttonElm = 'button#logout-button';
        $this->waitForElement($buttonElm);
        $this->i->click($buttonElm);
        $this->waitForElementNotVisible($buttonElm);

        $notLoggedOut = true;
        $i = 0;
        do {
            $i++;
            $this->i->wait(10);
            $url = $this->i->executeJS('return window.location.href');
            if (Str::contains($url, 'common/oauth2') || $i > 3) {
                $notLoggedOut = false;
            }
        } while($notLoggedOut);
    }

    /**
     * *************************************
     *             Conversation
     * *************************************
     */
    /**
     * @param string $user could be a user name, or a  UserId
     * @throws \Exception
     */
    public function openDmWith($user)
    {
        $selector = 'div.app-top-power-bar button#newchat-button';
        $this->waitForElement($selector);
        $this->i->click($selector);
        $inputElm = 'div.ts-people-picker input';
        $this->i->waitForElement($inputElm, $this->waitForElements);
        $this->i->click($inputElm);
        $this->i->sendKeys($user);
        $searchResult = '//div[contains(@class, "ts-result-container")]/li[contains(@class, "ts-resultRow") and contains(@aria-label, "'. $user .'")]';
        $this->i->waitForElement($searchResult, $this->waitForElements * 2);
        $this->i->click($searchResult);
        $this->waitForElement($this->selectorMessageInput);
        $this->i->click($this->selectorMessageInput);

        $this->waitForElement('//h2[@id="chat-header-title" and contains(text(), "'. $user .'")]');
    }

    /**
     * @param $team
     * @param $channel
     */
    public function openChannel($name)
    {
        $this->openTab('Teams');
        $team = $this->config('workspace.team_name');
        $teamSelector = 'div.app-left-rail-width div.team[data-tid="team-'. $team .'-li"]';
        $this->waitForElement($teamSelector, $this->waitForElements * 2 );
        $channelVisibleSelector = $teamSelector . ' a[title="'. $name .'"]';
        $viewHiddenSelector = $teamSelector . ' a[data-tid="team-'. $team .'-moreBtn"]';
        $channelsHiddenSelector = 'div.channel-list-overflow[aria-label="'. $team .'"] a[title="'. $name .'"]';

        // try search in visible channels first
        try {
            $this->i->waitForElement($channelVisibleSelector, 5);
            $this->i->click($channelVisibleSelector);
        } catch (\Exception $exception)
        {
            $this->i->click($viewHiddenSelector);
            $this->waitForElement($channelsHiddenSelector);
            $this->i->wait(1);
            $this->i->click($channelsHiddenSelector);
        }

        $this->i->wait(2);

        $headerSelector = '#chat-header-title h2.ts-title';
        $this->waitForElement($headerSelector);
        $this->i->see($name, $headerSelector);
    }


    public function leaveChannel($name, $count = 1)
    {
        $visibleChannels  = 'div.channels li.animate-channel-item a.channel-anchor';
        $showHiddenChannelsLinks = 'div.channels a[track-default="expandCollapseChannelOverflow"]';
        $hiddenChannelsSelector = 'ul.channel-list-overflow-channels li a.channel-anchor';
        $channels = [];
        $elms = $this->i->findElements($visibleChannels);
        if(is_array($elms) && !empty($elms)){

            foreach($elms as $elm) {
                $chanId = $elm->getAttribute('title');
                $channelName = $elm->getText();
                if (Str::startsWith($channelName, $name)) {
                    $channels[$chanId] = $channelName;
                    if (count($channels) >= $count)
                        break;
                }
            }

        }
        // try to collect hidden channels
        $links = $this->i->findElements($showHiddenChannelsLinks);
        foreach ($links as $elm) {
            $id = $elm->getAttribute('id');
            $this->i->click('div.channels a[id="'.$id.'"]');
            $this->i->waitForElementVisible('ul.channel-list-overflow-channels');
            $hiddenChannels = $this->i->findElements($hiddenChannelsSelector);

            foreach ($hiddenChannels as $hc) {
                $chanId = $hc->getAttribute('title');
                $channelName = $hc->getText();
                if (Str::startsWith($channelName, $name)) {
                    $channels[$chanId] = $channelName;

                    if (count($channels) >= $count)
                        break;
                }
            }

        }

        foreach ($channels as $channelId => $channelName)
        {
            $this->openChannel($channelName);
            $this->openChannelSetting();
            $deleteSelector = 'ul[data-tid="moreOptionsDropdown"] a[data-tid="deleteChannel"]';
            $this->i->waitForElement($deleteSelector);
            $this->i->click($deleteSelector);

            //confirm Delete
            $this->i->waitForElement('div.ts-modal-dialog');
            $this->i->waitForElement('div.ts-modal-dialog button#confirmButton');
            $this->i->click('div.ts-modal-dialog button#confirmButton');
            $this->i->wait(3);
            $this->i->waitForElement('button[track-summary="Add Member"]');
        }
    }

    public function createChannel($name)
    {
        $team = $this->config('workspace.team_name');
        $this->openTeamsOptions($team);
        $this->i->wait(2);
        $this->i->click('span[translate-once="team_add_channel"]');
        $this->i->waitForElementVisible("div.analytics-panel");
        $this->i->fillField('div.analytics-panel input[name="channelname"]', $name);
        $this->i->wait(2);
        $this->i->click("div.analytics-panel button.ts-btn-fluent-primary");
        $this->i->wait(4);
        $this->waitForElement('#chat-header-title h2[title="' . $name . '"]');
    }

    public function gotNewMessageBubbleInChat()
    {
        //
    }

    public function grabLastMessagesSince($n = 1, $timestamp = 0)
    {
        $elms = $this->i->findElements($this->selectorMessagesList);

        $messages = [];
        foreach ($elms as $elm)
        {
            $id = $elm->getAttribute('id');

            if ($id)
            {
                // @todo based on timestamp

                // find the first message
                $firstMessageElm = 'div#' . $id . ' thread-body[message-type="conversation"] div[data-tid="threadBodyMedia"]';
                $firstMessage    = $this->i->findElement($firstMessageElm);

                if ($firstMessage)
                {
                    $text          = $firstMessage->getText();
                    $messages[$id] = ['selector' => 'div#' . $firstMessage->getAttribute('id'), 'id' => $id, 'user' => ['id' => '', 'name' => '', 'type' => '', 'avatar' => ''], 'text' => $text, 'threads' => [], 'reactions' => []];

                    // reactions
                    $reactionsSelector = 'div#' . $firstMessage->getAttribute('id') . ' ul.message-reactions span.reaction-count';
                    $reactionElms = $this->i->findElements($reactionsSelector);

                    foreach ($reactionElms as $reactionElm)
                    {
                        $count = $reactionElm->getText();
                        $reactionName = str_replace('messageReactionCount-', '', $reactionElm->getAttribute('data-tid'));
                        $messages[$id]['reactions'][$reactionName] = $count;
                    }
                }

                 // search for threads
                $threadSelector = 'div#' .$id . ' thread-body[message-type="reply"] div[data-tid="threadBodyMedia"]';
                $threadElms = $this->i->findElements($threadSelector);
                foreach ($threadElms as $threadElm)
                {
                    $text          = $threadElm->getText();
                    $messages[$id]['threads'][] = [
                        'selector' => 'div#' . $threadElm->getAttribute('id'),
                        'id' => $id,
                        'user' => ['id' => '', 'name' => '', 'type' => '', 'avatar' => ''],
                        'text' => $text
                    ];
                }
            }
        }

        if ($n)
            $messages = array_splice($messages, -1 * $n);
        $messages = array_reverse($messages);

        return $messages;
    }


    /**
     * *************************************
     *             Messaging
     * *************************************
     */
    public function sendMessageInChat($message)
    {
        $this->waitForElement($this->selectorMessageInput);
        $this->i->click($this->selectorMessageInput);
        $this->i->wait(1);
        $this->i->sendKeys($message);
        $this->i->sendKeys(WebDriverKeys::ENTER);
    }

    public function seeTextInMessageInChat($findByText)
    {
        $this->i->assertIsArray($this->findMessageInChatMessage($findByText));
    }

    public function sendReactionChat($reaction, $findByText = null, $elm = null)
    {
        if ($findByText)
            $elm = $this->findElementInChatMessage($findByText);

        // hover over elm for reaction to show up
        $this->i->moveMouseOver($elm);

        //@todo various reactions
        $likeSelector = 'ul.message-reaction-container li.message-emoji-reaction[data-tid="'. $reaction . '"]';
        $this->waitForElement($likeSelector);
        $this->i->click($likeSelector);
    }

    public function seeReactionChat($reaction, $findByText = null, $elm = null, $count = null)
    {
        // @todo find by elm..
        $reactions = [];

        if ($findByText)
        {
            $match = $this->findMessageInChatMessage($findByText);
            foreach ((array) Arr::get($match, 'threads') as $thread)
            {
                $text = $thread['text'];
                if (preg_match('/replied with (.+?)/', $text, $matches))
                {
                    $reaction = array_pop($matches);
                    if (!isset($reactions[$reaction]))
                        $reactions[$reaction] = 0;
                    $reactions[$reaction]++;
                }
            }
        }

        $this->i->assertArrayHasKey($reaction, $reactions);
    }

    public function editMessageInChat($newMessage, $findByText = null, $elm = null)
    {
        if ($findByText)
            $elm = $this->findElementInChatMessage($findByText);

        // hover over elm for reaction to show up
        $this->i->moveMouseOver($elm);
        $this->waitForElement($this->selectorMessageMoreMenu);
        $this->i->click($this->selectorMessageMoreMenu);
        $this->waitForElement($this->selectorMessageMenuEdit );
        $this->i->click($this->selectorMessageMenuEdit);

        $this->waitForElement($elm . ' ' . $this->selectorMessageEditInput);
        $this->i->sendKeys($newMessage);
        $this->i->sendKeys(WebDriverKeys::ENTER);
    }

    public function deleteMessageInChat($findByText = null, $elm = null)
    {
        if ($findByText)
            $elm = $this->findElementInChatMessage($findByText);

        $this->i->moveMouseOver($elm);
        $this->waitForElement($this->selectorMessageMoreMenu);
        $this->i->click($this->selectorMessageMoreMenu);
        $this->waitForElement($this->selectorMessageMenuDelete );
        $this->i->click($this->selectorMessageMenuDelete);

        $this->waitForText('This message has been deleted');
    }

    public function threadMessageInChat($threadMessage, $findByText = null, $elm = null)
    {
        if ($findByText)
            $elm = $this->findElementInChatMessage($findByText);

        $threadElm = $elm . ' button[data-tid="replyMessageButtonShort"]';
        $this->waitForElement($threadElm);
        $this->i->click($threadElm);

        $replyInputElm = $elm . ' ' . $this->selectorMessageEditInput;
        $this->waitForElement($replyInputElm);
        $this->i->click($replyInputElm);
        $this->i->sendKeys($threadMessage);
        $this->i->sendKeys(WebDriverKeys::ENTER);

        $this->waitForText($threadMessage);
    }

    public function seeThreadMessageInChat($threadText, $findByText = null, $elm = null)
    {
        $message = [];

        // if finding by text
        if ($findByText)
            $message = $this->findMessageInChatMessage($findByText);

        if (is_array($message) && count($message['threads']))
        {
            foreach ($message['threads'] as $thread)
            {
                $text = $thread['text'];
                if (Str::contains($text, $threadText))
                {
                    $matched = true;
                    break;
                }
            }
        }
        $this->i->assertEquals(true, $matched);
    }

    public  function seeMentionInMessageInChat($text, $mentionUser)
    {
        $match = null;
        $text = substr($text, 1);
        $messageSelector = $this->findElementInChatMessage($text);
        $selector = $messageSelector . ' .mention-me';
        $mention = $this->i->findElement($selector);
        $mentionText = $mention->getText();
        if (Str::contains($mentionText, '@'.$mentionUser)) {
            $match = $mentionUser;
        }
        return $match;
    }

    protected function findMessageInChatMessage($findByText)
    {
        $match = null;
        $messages = $this->grabLastMessagesSince(0);
        foreach ($messages as $id => $message)
        {
            $text = $message['text'];
            if (Str::contains($text, $findByText)) {
                $match = $message;
                break;
            }
        }

        return $match;
    }

    protected function findElementInChatMessage($findByText)
    {
        $message = $this->findMessageInChatMessage($findByText);

        return Arr::get($message, 'selector');
    }

    /**
     * *************************************
     *             Files
     * *************************************
     */
    public function sendFileInChat(array $files, $message)
    {
        throw new Exception('Not implemented');
    }

    public function seeFileInChat(array $files, $findByText)
    {
        throw new Exception('Not implemented');
    }



    ###########
    ##### helpers
    ###
    public function waitForAppToLoad()
    {
        $url = $this->i->executeJS('return window.location.href');
        if (!Str::contains($url, 'teams.microsoft.com'))
            $this->i->amOnUrl($this->workspaceUrl() . '/_#');

        // if url has /package/go..
        if (Str::contains($url, '/_package/go'))
            $this->i->amOnUrl($this->workspaceUrl() . '/_#');

        $this->i->wait(2);
        $this->waitForElement('.powerbar-profile');

        // Use the web app instead
        try {
            $this->i->waitForText('Use the web app instead', 5);
            $this->i->click('Use the web app instead');
            $this->i->wait(1);
        } catch (\Exception $exception) {}
    }

    /**
     * @param $team
     */
    protected function openTeamsOptions($team)
    {
        $this->openTab('Teams');
        $this->i->wait(1);
        $teamElement = 'ul[aria-label="Teams and Channels list"]  a[data-tid="team-' . $team . '"] button[title="More options"]';
        $this->i->click($teamElement);
        $menuElem  = 'ul[aria-label="Team ' . $team . ' actions"]';
        $this->waitForElement($menuElem);
    }

    /**
     * @pram $appName
     */
    public function openAppExtentionsInChat($appName){
        //please do not remove wait() from this method, need to wait for angular response
        $this->i->wait(4);
        $this->i->click('button.showmore-options', 'div.extension-icons-container');
        $this->waitForElement('div.v2-input-extensions-selector');
        $this->i->wait(2);
        $this->i->fillField("input.input-extensions-selector-search-box", $appName);
        $this->i->wait(3);
        $elem = 'span[title="' . $appName . '"]';
        $this->i->click($elem);
        $this->i->wait(4);
    }

    /**
     * @param $tab
     */
    public function openTab($tab)
    {
        $this->waitForElement('#teams-app-bar');
        $this->i->click($tab, "#teams-app-bar");
    }

    public function closePopup()
    {
        $this->i->click('div.close-container button.icons-close');
    }

    protected function openChannelSetting()
    {
        $this->i->click('button#settingsDropdown');
        $this->i->waitForElementVisible('ul[data-tid="moreOptionsDropdown"]');
    }

    protected function isUserId($name)
    {
        return false;
    }

    protected function isChannelId($name)
    {
        return false;
    }
}