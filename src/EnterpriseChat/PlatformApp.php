<?php

namespace ReliableOffshore\CodeceptionChatApps\EnterpriseChat;

use Codeception\Actor;
use Codeception\Lib\ModuleContainer;
use Codeception\Module\WebDriver;
use Illuminate\Support\Arr;

abstract class PlatformApp
{
    /** @var Actor|WebDriver|ModuleContainer|\ReliableOffshore\CodeceptionApps\Modules\WebDriverModule|\Codeception\Module\Asserts */
    protected $i;
    public $config;

    protected $domain;
    protected $username;
    protected $password;

    protected $waitForElements = 45;

    public function __construct(Actor $i, $configFile)
    {
        $this->i = $i;
        $this->init($configFile);
    }

    public function config($key, $value = null)
    {
        // @todo allow setting values
        return Arr::get($this->config, $key);
    }

    protected function init($configFile)
    {
        if (file_exists($configFile)) {
            $this->config = include $configFile;
            $this->domain = Arr::get($this->config, 'workspace.domain');
            $this->waitForElements = Arr::get($this->config, 'webdriver.waitForElements', $this->waitForElements);
        }
        else
            throw new \Exception("Config file not found: $configFile");
    }

    abstract public function install($url = null);

    abstract public function login($path = null);

    abstract public function logout();

    abstract public function openDmWith($user);

    abstract public function openChannel($name);

    abstract public function leaveChannel($name);

    abstract public function createChannel($name);

    abstract public function sendMessageInChat($message);

    abstract public function seeTextInMessageInChat($findByText);

    abstract public function editMessageInChat($newMessage, $findByText = null, $elm = null);

    abstract public function deleteMessageInChat($findByText = null, $elm = null);

    abstract public function sendReactionChat($reaction, $findByText = null, $elm = null);

    abstract public function seeReactionChat($reaction, $findByText = null, $elm = null, $count = null);

    abstract public function threadMessageInChat($threadMessage, $findByText = null, $elm = null);

    abstract public function seeThreadMessageInChat($threadText, $findByText = null, $elm = null);

    abstract public function sendFileInChat(array $files, $message);

    abstract public function seeFileInChat(array $files, $findByText);

    abstract public function waitForAppToLoad();


    public function persona($name)
    {
        $username = $password = null;
        $domain = $this->domain;
        if ($persona = Arr::get($this->config, "workspace.persona.$name"))
        {
            $username = Arr::get($persona, 'email');
            $password = Arr::get($persona, 'password');
        }

        if (!$domain || !$username || !$password)
            throw new \Exception("Persona ($name) or domain ($domain) not found");

        $this->domain = $domain;
        $this->username = $username;
        $this->password = $password;
    }

    protected function workspaceUrl()
    {
        return 'https://' . $this->domain;
    }

    public function waitForElement($elm)
    {
        $this->i->waitForElement($elm, $this->waitForElements);
    }

    public function waitForElementNotVisible($elm)
    {
        $this->i->waitForElementNotVisible($elm, $this->waitForElements);
    }

    public function waitForText($text, $selector = null)
    {
        $this->i->waitForText($text, $this->waitForElements, $selector);
    }

    public function runningOnMac()
    {
        return stripos(PHP_OS, 'darwin') === 0;
    }
}
