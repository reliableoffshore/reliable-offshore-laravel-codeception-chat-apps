<?php

namespace ReliableOffshore\CodeceptionChatApps\EnterpriseChat\Slack;


use Codeception\Actor;
use Codeception\Util\Locator;
use function Composer\Autoload\includeFile;
use Facebook\WebDriver\WebDriverKeys;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use ReliableOffshore\CodeceptionChatApps\EnterpriseChat\PlatformApp;

class SlackApp extends PlatformApp
{
    // selector for App (bot) home tab
    public $selectorAppTab        = '.p-workspace__primary_view_contents button[data-qa="%s"]';
    public $selectorChatAppLoaded = 'i[data-qa="presence_indicator"]';
    public $selectorPopupGoButton = 'button[data-qa="go_button"]';
    public $selectorPopupNextButton = 'button[data-qa="wizard_modal_next"]';
    public $selectPopupClose = 'button[data-qa="wizard_modal_close"]';
    public $selectorMessageInput = 'div[data-qa="message_input"]';
    public $selectorMessageAttachment = 'button[data-qa="msg_input_file_btn_inset"]';
    public $selectorMessagesList = '.p-message_pane';
    public $selectorMessageLast  = '.p-message_pane div[data-qa="slack_kit_list"] > div[data-qa="virtual-list-item"]:last-child';
    public $selectorMessageReaction = 'button[data-qa="add_reaction_action"]';
    public $selectorMessageThread = 'button[data-qa="start_thread"]';
    public $selectorMessageMoreMenu = 'button[data-qa="more_message_actions"]';
    public $selectorCommandActionLogin = '.p-actions_block_elements button:first-of-Type';
    public $selectorBrowseChannels = 'div[data-qa="channel_browser_title"]';
    public $selectBrowseInput       = 'input[data-qa="browse_page_search_input"]';
    public $selectorSearchJumpTo     = 'div[data-qa="search_input_box"]';
    public $selectorSearchDMBrowser  = '#dm-browser';
    public $selectorConversionHeader = '//button[@data-qa="channel_name"]';
    public $selectorConversionHeaderSearch = '//button[@data-qa="channel_name" and contains(text(), "%s")]';
    public $selectorEmojiSearchInput = 'input[data-qa="emoji_picker_input"]';
    public $selectorEmojiSearchPreview = 'span[data-qa="emoji_preview_aliases"]';
    public $selectorMessageMenuEdit = '.ReactModalPortal button[data-qa="edit_message"]';
    public $selectorMessageMenuDelete = '.ReactModalPortal button[data-qa="delete_message"]';
    public $selectorMessageEditInput = 'div[data-qa="message_editor"] div.ql-editor p';
    public $selectorDialogGo         = 'button[data-qa="dialog_go"]';
    public $selectorPaneRightClose   = 'button[data-qa="close_flexpane"]';
    public $selectorMessagesThreadList = '.p-flexpane__body div[data-qa="slack_kit_list"] div[data-qa="virtual-list-item"]';
    public $selectorMessageThreadInput = '.p-flexpane__body div[data-qa="message_input"]';
    public $selectorFileMessageInput = 'div.p-file_upload_dialog__section div.ql-editor p';

    ###########
    ##### auth
    ###
    public function install($url = null)
    {
        if ($url)
            $this->i->amOnUrl($url);

        // @todo have a better way to detect if logged on
        try
        {
            $this->i->waitForElement("#domain", 15);
            $this->i->fillField("#domain", str_replace('.slack.com', '', $this->domain));
            $this->i->click('#submit_team_domain');

            // on next page
            $this->login('install');
        } catch (\Exception $e) {}

        // on next page
        $this->waitForElement('//button[@data-qa="oauth_submit_button"]');
        $this->i->click('//button[@data-qa="oauth_submit_button"]');
        $this->waitForElementNotVisible('#oauth_submit_button');
    }

    public function login($path = null)
    {
        if (!$path)
            $this->i->amOnUrl($this->workspaceUrl());

        $this->waitForElement('#email');
        $this->i->fillField('#email', $this->username);
        $this->i->fillField('#password', $this->password);
        $this->i->click('#signin_btn');

        // when path is not install, lets wait for page to load
        if ($path != 'install') {
            $this->waitForAppToLoad();
        }
    }

    public function logout()
    {
        $menuElm = 'div[data-qa="sidebar_header_button"]';
        $this->waitForElement($menuElm);
        $this->i->click($menuElm);

        $this->waitForText('Sign out of');
        $this->i->click('Sign out of');
        $this->waitForElementNotVisible($menuElm);
    }


    ###########
    ##### conversations
    ###
    /**
     * @param string $user could be a user name, or a slack UserId (e.g. D012F7TL5CN)
     * @throws \Exception
     */
    public function openDmWith($user)
    {
        if ($this->isUserId($user))
        {
            // @todo check if already in current dm
            $url = "https://app.slack.com/client/{$this->getWorkspaceId()}/{$user}";
            $this->i->amOnUrl($url);
        }
        else
        {
            // @todo check if already in current dm
            if ($this->runningOnMac())
                $this->i->sendKeys([WebDriverKeys::COMMAND, WebDriverKeys::SHIFT, 'K']);
            else
                $this->i->sendKeys([WebDriverKeys::CONTROL, WebDriverKeys::SHIFT, 'K']);
            $this->waitForElement($this->selectorSearchDMBrowser);
            $this->i->fillField($this->selectorSearchDMBrowser, $user);

            $searchResultElm = '//span[@data-qa="member-entity__primary-name"]/*/strong[contains(text(),"' . $user .'")]';
            $this->waitForElement($searchResultElm);
            $this->i->click($searchResultElm);
            $this->waitForElement($this->selectorPopupGoButton);
            $this->i->click($this->selectorPopupGoButton);

            // @todo check the name of chat window as well
        }

        // if url has /app in the end, then we are chatting with bot
        $uri = $this->i->grabFromCurrentUrl();
        if (Str::endsWith($uri, '/app'))
            $this->openAppTab('messages');

        $this->waitForElement($this->selectorMessageInput);
    }

    public function openChannel($name)
    {
        $isChannelId = false;
        if ($this->isChannelId($name))
        {
            $id = $name;
            $isChannelId = true;
        }
        else
        {
            // @todo check if already in channel
            if ($this->runningOnMac())
                $this->i->sendKeys([WebDriverKeys::COMMAND, WebDriverKeys::SHIFT, 'L']);
            else
                $this->i->sendKeys([WebDriverKeys::CONTROL, WebDriverKeys::SHIFT, 'L']);

            $this->waitForElement($this->selectorBrowseChannels);
            $this->i->fillField($this->selectBrowseInput, $name);
            $this->i->sendKeys([WebDriverKeys::ENTER]);

            $searchResultElm = 'div.p-browse_page_results div[data-qa="slack_kit_list"] > div.c-virtual_list__item:last-child';
            try
            {
                $this->i->waitForElement($searchResultElm, 15);
            } catch (\Exception $exception)
            {
                // sometimes search fails, so trying a fix for that
                $this->i->wait(130);
                $this->i->reloadPage();
                $this->waitForElement($this->selectorBrowseChannels);
                $this->i->fillField($this->selectBrowseInput, $name);
                $this->i->sendKeys([WebDriverKeys::ENTER]);
                $this->waitForElement($searchResultElm);
            }

            $elms = $this->i->findElements($searchResultElm);
            foreach ($elms as $elm)
            {
                $id = $elm->getAttribute('id');
                break;
            }
        }

        $workspaceId = $this->getWorkspaceId();
        $this->i->amOnUrl("https://app.slack.com/client/$workspaceId/$id");
        $this->waitForAppToLoad();

        // we should be inside the channel now
        if (!$isChannelId)
        {
            $channelNameHeader = sprintf($this->selectorConversionHeaderSearch, $name);
            $this->waitForElement($channelNameHeader);
        }
    }

    public function leaveChannel($name, $count = 1)
    {
        $selector  = 'div.p-channel_sidebar__static_list__item a.p-channel_sidebar__channel';
        $elms = $this->i->findElements($selector);

        $channels = [];
        foreach($elms as $elm)
        {
            $chanId = $elm->getAttribute('data-qa-channel-sidebar-channel-id');
            $channelName = $elm->getText();
            if (Str::startsWith($channelName, $name))
            {
                $channels[$chanId] = $channelName;

                if (count($channels) >= $count)
                    break;
            }
        }

        foreach ($channels as $channelId => $channelName)
        {
            $this->openChannel($channelId);

            $channelNameHeader = sprintf($this->selectorConversionHeaderSearch, $name);
            $this->waitForElement($channelNameHeader);

            $this->i->sendKeys('/leave');
            $this->i->sendKeys(WebDriverKeys::ENTER);
            $this->i->click('button[data-qa="texty_send_button"]');

            // sometimes we see: Are you sure you want to archive
            try {
                $this->i->waitForText('Are you sure you want to archive', 3);
                $this->i->sendKeys(WebDriverKeys::ENTER);
                $this->i->wait(3);
            } catch (\Exception $exception)
            {
            }
            $this->i->wait(1);
        }
    }

    public function createChannel($name)
    {
        $elmPlus = 'button[data-qa="channel_sidebar__plus__channels"]';
        $this->waitForElement($elmPlus);
        $this->i->click($elmPlus);

        $this->waitForElement('div[data-qa="menu_items"]');
        $this->i->click('div[data-qa="menu_items"] div.c-menu_item__li:last-child');

        $inputNameElm = 'input[data-qa="channel-name-input"]';
        $this->waitForElement($inputNameElm);
        $this->i->fillField($inputNameElm, $name);
        $this->i->wait(1);
        $this->i->click('button[data-qa="channel_create_modal_cta"]');

        // wait for success popup
        $this->i->wait(1);
        $this->waitForText($name);
        try
        {
            $this->i->click('button[data-qa="sk_close_modal_button"]');
        }catch (\Exception $e) {}
    }

    public function gotNewMessageBubbleInChat()
    {
        $this->waitForElement('//hr[@class="c-message_list__unread_divider__separator"]');
    }

    public function grabLastMessagesSince($n = 1, $timestamp = 0, $selector = null)
    {
        if (!$selector)
            $selector = $this->selectorMessagesList . ' div[data-qa="slack_kit_list"] div[data-qa="virtual-list-item"]';

        $this->waitForElement($selector);
        $elms = $this->i->findElements($selector);

        $messages = [];
        foreach ($elms as $elm)
        {
            $id = $elm->getAttribute('id');
            $idInt = intval($id);

            // regular messages have a numeric id, where as threads id is of format: G014BF6LAJH-1590769679.002400-thread-list_1590769684.002500
            if ($idInt || preg_match('/thread-list_\d+.\d+/', $id))
            {
                if ($timestamp && $idInt <= $timestamp)
                    continue;

                $selector = $this->selectorMessagesList . ' div[id="'. $id .'"]';
                // @todo parse user, user_id & app from
                //<div class="c-message_kit__gutter__right" data-qa="message_content"><span class="c-message__sender c-message_kit__sender" data-qa="message_sender" data-stringify-type="replace"
                //  data-stringify-text="user_name">
                //  <a target="_blank" rel="noopener noreferrer"
                //      data-message-sender="USERID"
                //  data-qa="message_sender_name" aria-haspopup="true" class="c-link c-message__sender_link "
                //          href="/team/sksksks">USER_NAME</a>
                //      <span class="margin_left_25"><span class="c-app_badge" data-stringify-suffix=" ">APP</span></span>


                //data-qa="message_sender_name"
                $text = $elm->getText();
                $messages[$id] = [
                                    'selector' => $selector,
                                    'id' => $id,
                                    'user' => [
                                        'id' => '',
                                        'name' => '',
                                        'type' => '',
                                        'avatar' => ''
                                    ],
                                    'text' => $text];
            }
        }

        if ($n)
            $messages = array_splice($messages, -1 * $n);
        $messages = array_reverse($messages);

        return $messages;
    }

    /**
     * @param string $name
     * @throws \Exception
     */
    public function openAppTab($name)
    {
        $selector = sprintf($this->selectorAppTab, $name);
        $this->waitForElement($selector);
        $this->i->click($selector);
    }




    ###########
    ##### Messaging
    ###
    public function sendMessageInChat($message)
    {
        $this->waitForElement($this->selectorMessageInput);
        $this->i->sendKeys($message);
        $this->i->sendKeys(WebDriverKeys::ENTER);
    }

    public function seeTextInMessageInChat($findByText)
    {
        $this->i->assertIsArray($this->findMessageInChatMessage($findByText));
    }

    public function sendReactionChat($reaction, $findByText = null, $elm = null)
    {
        // if finding by text
        if ($findByText)
            $elm = $this->findElementInChatMessage($findByText);

        // hover over elm for reaction to show up
        $this->i->moveMouseOver($elm);
        $reactionElm = $elm . ' ' . $this->selectorMessageReaction;
        $this->waitForElement($reactionElm);
        $this->i->click($reactionElm);
        $this->i->fillField($this->selectorEmojiSearchInput, $reaction);
        $this->waitForElement($this->selectorEmojiSearchPreview);
        $this->i->pressKey($this->selectorEmojiSearchInput, WebDriverKeys::ENTER);
    }

    public function seeReactionChat($reaction, $findByText = null, $elm = null, $count = null)
    {
        // if finding by text
        if ($findByText)
            $elm = $this->findElementInChatMessage($findByText);

        // got reactions - dont fail on them..?
        $reactions = [];
        try
        {
            $reactionElms = $this->i->findElements("$elm button[data-qa='reactji']");
            foreach ($reactionElms as $reactionElm)
            {
                $reactionName  = $this->i->grabAttributeFrom("$elm img.c-emoji", 'data-stringify-emoji');
                $reactionCount = intval($this->i->grabTextFrom("$elm span.c-reaction__count", 'innerHtml'));
                $reactions[$reactionName] = $reactionCount;
            }
        } catch (\Exception $e) {}


        $this->i->assertArrayHasKey($reaction, $reactions);

        if ($count)
            $this->i->assertSame($count, $reactions[$reaction]);
    }

    public function editMessageInChat($newMessage, $findByText = null, $elm = null)
    {
        if ($findByText)
            $elm = $this->findElementInChatMessage($findByText);

        // hover over elm for reaction to show up
        $this->i->moveMouseOver($elm);
        $this->i->click($elm . ' ' . $this->selectorMessageMoreMenu);
        $this->waitForElement($this->selectorMessageMenuEdit);
        $this->i->click($this->selectorMessageMenuEdit);

        $this->waitForElement($this->selectorMessageEditInput);
        $this->i->sendKeys($newMessage);
        $this->i->sendKeys(WebDriverKeys::ENTER);
    }

    public function deleteMessageInChat($findByText = null, $elm = null)
    {
        if ($findByText)
            $elm = $this->findElementInChatMessage($findByText);

        // hover over elm for reaction to show up
        $this->i->moveMouseOver($elm);
        $this->i->click($elm . ' ' . $this->selectorMessageMoreMenu);
        $this->waitForElement($this->selectorMessageMenuEdit);
        $this->i->click($this->selectorMessageMenuDelete);

        $this->waitForElement($this->selectorDialogGo);
        $this->i->click($this->selectorDialogGo);
        $this->waitForElementNotVisible($this->selectorDialogGo);
    }

    public function threadMessageInChat($threadMessage, $findByText = null, $elm = null)
    {
        if ($findByText)
            $elm = $this->findElementInChatMessage($findByText);

        // hover over elm for reaction to show up
        $this->i->moveMouseOver($elm);
        $reactionThread = $elm . ' ' . $this->selectorMessageThread;
        $this->waitForElement($reactionThread);
        $this->i->click($reactionThread);
        $this->waitForElement($this->selectorMessageThreadInput);
        $this->i->click($this->selectorMessageThreadInput);
        $this->i->sendKeys($threadMessage);
        $this->i->sendKeys(WebDriverKeys::ENTER);
        $this->i->click($this->selectorPaneRightClose);
    }

    public function seeThreadMessageInChat($threadText, $findByText = null, $elm = null)
    {
        // if finding by text
        if ($findByText)
            $elm = $this->findElementInChatMessage($findByText);

        $threads = [];
        $elmReplyBar = $elm . ' div[data-qa="reply_bar"]';
        $this->waitForElement($elmReplyBar);
        $this->i->click($elmReplyBar);
        try
        {
            $threads = $this->grabLastMessagesSince(0, null, $this->selectorMessagesThreadList);
        } catch (\Exception $e) {}

        $matched = false;
        foreach ($threads as $thread)
        {
            $text = $thread['text'];
            if (Str::contains($text, $threadText))
            {
                $matched = true;
                break;
            }
        }

        $this->i->assertEquals(true, $matched);
        $this->i->click($this->selectorPaneRightClose);
    }

    protected function findElementInChatMessage($findByText)
    {
        $message = $this->findMessageInChatMessage($findByText);

        return Arr::get($message, 'selector');
    }

    protected function findMessageInChatMessage($findByText)
    {
        $match = null;
        $messages = $this->grabLastMessagesSince(0);
        foreach ($messages as $id => $message)
        {
            $text = $message['text'];
            if (Str::contains($text, $findByText)) {
                $match = $message;
                break;
            }
        }

        return $match;
    }

    public  function seeMentionInMessageInChat($text, $mentionUser)
    {
        $match = null;
        $messageSelector = $this->findElementInChatMessage($text);
        $selector = $messageSelector . ' .c-member_slug--mention';
        $mention = $this->i->findElement($selector);
        $mentionText = $mention->getText();
        if (Str::contains($mentionText, '@'.$mentionUser)) {
            $match = $mentionUser;
        }

        return $match;
    }

    ###########
    ##### Files
    ###
    public function sendFileInChat(array $files, $message)
    {
        // upload file
        foreach ($files as $file)
        {
            $elm = 'input[data-qa="file_upload"]';
            $this->i->attachFile($elm, $file);
            $this->i->wait(1);
        }

        // any message?
        if ($this->i->findElement($this->selectorFileMessageInput))
            $this->i->click($this->selectorFileMessageInput);
        else
            $this->i->click($this->selectorMessageInput);

        $this->i->sendKeys($message);
        $this->i->wait(1);
        $this->i->sendKeys(WebDriverKeys::ENTER);

        // wait for upload to finish
        try
        {
            $this->i->waitForElementNotVisible('.p-file_upload_banner__progress', $this->waitForElements * 2);
        } catch (\Exception $e) {}
    }

    public function seeFileInChat(array $files, $findByText)
    {
        $message = $this->findMessageInChatMessage($findByText);

        $_files = [];
        if ($message && ($elm = $message['selector']))
        {
            $elms = $this->i->findElements($elm . ' a[data-qa="download_action"].c-link');
            foreach ($elms as $elm)
            {
                $href = $elm->getAttribute('href');
                $name = basename($href);
                if (Str::contains($href, 'files.slack.com'))
                    $_files[$href] = $name;
            }
        }

        $matched = 0;
        foreach ($files as $file)
        {
            foreach ($_files as $url => $name)
            {
                if ($name == $file)
                    $matched++;
            }
        }

        $this->i->assertCount($matched, $files);
    }

    ###########
    ##### interactive
    ###
    public function fillOptionsList($actionId, $type)
    {
        //.input[data-qa="{$actionId}-input"

    }
    public function grabOptionsFromList($actionId)
    {
        $this->waitForElement('div[data-qa="' . $actionId .'-options-list"]');
    }

    /**
     * @param $action
     */
    public function clickActionButtonInLastMessage($action)
    {
        $this->i->click($this->selectorMessageLast . ' button[data-qa-action-id="' . $action . '"]');
    }

    /**
     * @param $selector
     * Making another universel function to click element in  DM
     * because of may of elements changes their Id's on each refresh
     */
    public function clickCSSElementInLastMessage($selector)
    {
        $this->i->click($this->selectorMessageLast . " " . $selector);
    }




    ###########
    ##### helpers
    ###
    /**
     * @throws \Codeception\Exception\ModuleException
     * @throws \Exception
     */
    public function waitForAppToLoad()
    {
        $host = $this->i->executeJS('return window.location.host');
        if (!Str::contains($host, 'app.slack.com'))
        {
            $this->i->amOnUrl($this->workspaceUrl());
            $this->waitForElement($this->selectorChatAppLoaded);
        }
        else {
            $this->waitForElement($this->selectorChatAppLoaded);
        }

        $this->clearPopups();
    }

    public function getWorkspaceId()
    {
            // grab client it from url
            $clientId = $this->i->grabFromCurrentUrl('/client\/(\w+)/');
            if ($clientId)
            {
                $workspaceId = $clientId;
            }
            else
            {
                $workspaceId = $this->i->grabAttributeFrom('//div[@data-team-id]', 'data-team-id');
            }

        return $workspaceId;
    }

    public function clearPopups()
    {
        // @todo make it smarter

        // reset any dialog popups
        $this->i->sendKeys(WebDriverKeys::ESCAPE);
        $this->i->sendKeys(WebDriverKeys::ESCAPE);
        $this->i->sendKeys(WebDriverKeys::ESCAPE);
    }

    protected function isUserId($name)
    {
        return preg_match('/^(U|W)(.*\d+.*|[A-Z]+)$/', $name);
    }

    protected function isChannelId($name)
    {
        $chanId = false;
        if (preg_match('/^([A-Z0-9]+)$/', $name))
            $chanId = true;

        return $chanId;
    }
}
