<?php

namespace ReliableOffshore\CodeceptionChatApps\EnterpriseChat\Webex;


use Codeception\Actor;
use Codeception\Util\Locator;
use function Composer\Autoload\includeFile;
use Facebook\WebDriver\WebDriverKeys;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use ReliableOffshore\CodeceptionChatApps\EnterpriseChat\PlatformApp;

class WebexApp extends PlatformApp
{
    /** Webex Selectors */
    public $selectorSpaceListArea = '.space-list-section';
    public $selectorSearchChannelsBtn = '.space-list-header button[title="Open Search Section"]';
    public $selectorSearchInput  = '#search-composer';
    public $selectorMessagesList = '.room-content #activity-items div.activity-item';
    public $selectorAppLastDM    = '#activity-list div[data-group-pos="additional"]:last-of-type';
    public $selectorAppStartupMsg = '#activity-list div[data-group-pos="start"]:last-of-type';
    public $selectorMessageInput = '.message-composer-container .ql-editor p';
    public $selectorChatAppLoaded = '#convo-section .md-list .md-avatar';
    public $selectorConversationHeaderTitle = '.activity-title-bar #activityMenuButton';
    public $selectorAvatarMenu = 'div.avatar-settings-menu-section .md-avatar';
    public $selectorSignOut = 'div.avatar-settings-menu-section div[title="Sign Out"]';
    public $selectorMessageMenuDelete = 'button.delete-button';
    public $selectorMessageMenuDeleteConfirm = 'button.confirmation-modal-action';
    public $selectorMessageMenuThread = 'button.thread-button';
    public $selectorThreadMessageSend = 'div.composer-action-section button[alt="Send"]';

    public function install($url = null)
    {
        if ($url)
            $this->i->amOnUrl($url);

        // @todo have a better way to detect if logged on
        try
        {
            $this->login('install');

            // for first time install, we are on the screen asking for permissions to install
            try
            {
                $elm = 'button.cui-button--blue';
                $this->i->waitForElement($elm, 15);
                $this->i->click($elm);
            } catch (\Exception $e)  {}
        } catch (\Exception $e) {}

        // we are redirected to success page
    }

    public function login($path = null)
    {
        if (!$path)
        {
            $this->i->amOnUrl($this->workspaceUrl());
            try{
                $this->waitForElement('.md-input[autocomplete="email"]');
                $this->i->fillField('.md-input[autocomplete="email"]', $this->username);
                $this->waitForElement('form .md-button[alt="Next"]');
                $this->i->click('form .md-button[alt="Next"]');
            }catch (\Exception $e){}

            try{
                $this->waitForElement('#IDToken2');
                $this->i->fillField('#IDToken2', $this->password);
                $this->i->click('#Button1');
                $this->waitForAppToLoad();
            }catch (\Exception $e){}
        }
        // login via install
        else
        {
            $this->i->waitForElement('#IDToken1', 15);
            $this->i->fillField('#IDToken1', $this->username);
            $this->waitForElement('#IDButton2');
            $this->i->click('#IDButton2');
            $this->waitForElement('#IDToken2');
            $this->i->fillField('#IDToken2', $this->password);
            $this->i->click('#Button1');
        }
    }

    public function logout()
    {
        $this->i->click($this->selectorAvatarMenu);
        $this->waitForElement($this->selectorSignOut);
        $this->i->click($this->selectorSignOut);
        $this->waitForElementNotVisible($this->selectorSignOut);
    }



    /**
     * *************************************
     *             Conversations
     * *************************************
     */
    public function openDmWith($user)
    {
        if ($this->isUserId($user))
        {
            // @todo
        }
        else
        {
            $this->waitForElement($this->selectorSearchChannelsBtn);
            $this->i->click($this->selectorSearchChannelsBtn);
            $this->waitForElement($this->selectorSearchInput);
            $this->i->fillField($this->selectorSearchInput, $user);
            $this->i->wait(1);
            $searchResultElm = '.md-list-item.md-list-item--space[title="' . $user . '"]';
            $this->waitForElement($searchResultElm);;
            $this->i->click($searchResultElm);

            // make sure we are inside the right space
            $this->waitForElement('.activity-menu-button-section button[title="' . $user . '"]');
            $this->waitForElement($this->selectorMessageInput);
        }
    }

    public function openChannel($name)
    {
        if ($this->isChannelId($name))
        {
            // @todo
        }
        else
        {
            $this->i->wait(5);
            $this->waitForElement($this->selectorSearchChannelsBtn);
            $this->i->click($this->selectorSearchChannelsBtn);
            $this->waitForElement($this->selectorSearchInput);
            $this->i->fillField($this->selectorSearchInput, $name);

            $searchResultElm = '.md-list-item.md-list-item--space[title="' . $name . '"]';
            $this->waitForElement($searchResultElm);;
            $this->i->click($searchResultElm);

            // make sure we are inside the right space
            $this->waitForElement('.activity-menu-button-section button[title="' . $name . '"]');
            $this->waitForElement($this->selectorMessageInput);
        }
    }

    public function leaveChannel($name, $count = 1)
    {
        $selector  = 'div.space-list-wrapper div.md-list-item__header';
        $elms = $this->i->findElements($selector);

        $channels = [];
        foreach($elms as $elm)
        {
            $chanId = $elm->getAttribute('data-qa-channel-sidebar-channel-id');
            $channelName = $elm->getText();
            $chanId = $channelName;
            if (Str::startsWith($channelName, $name))
            {
                $channels[$chanId] = $channelName;

                if (count($channels) >= $count)
                    break;
            }
        }

        foreach ($channels as $channelId => $channelName)
        {
            $this->i->click('div.space-list-wrapper div[aria-label="'. $channelName .'"]');

            $roomMenuElm = 'button#room-actions-menu-button';
            $this->waitForElement($roomMenuElm);
            $this->i->click($roomMenuElm);

            $leaveElm = 'div.room-actions-menu--leave';
            $this->waitForElement($leaveElm);
            $this->i->click($leaveElm);

            // confirm dialog
            $confirmElm = 'button[aria-label="Leave space"]';
            $this->waitForElement($confirmElm);
            $this->i->click($confirmElm);

            $this->waitForElementNotVisible($confirmElm);
        }
    }


    public function createChannel($name)
    {

    }

    public function gotNewMessageBubbleInChat()
    {
        $element = ".md-list-separator__container";
        $this->waitForElement($element);
    }

    public function grabLastMessagesSince($n = 1, $timestamp = 0)
    {
        $this->waitForElement($this->selectorMessagesList);
        $elms = $this->i->findElements($this->selectorMessagesList);

        $messages = [];
        $lastMessageId = null;
        foreach ($elms as $elm)
        {
            $id = $elm->getAttribute('id');
            if ($id)
            {
                // @todo return messages based on timestamp

                // @todo parse user, app, time, avatar etc
//                <img alt="" class="md-avatar__img" draggable="false" src="https://avatar-prod-us-east-2.webexcontent.com/Avtr~V1~6ff87596-086e-4e1e-bd5e-2421544490c7/V1~e3cc1d07-0e33-4975-9aab-daea633765db~bd6f06aa754e4040bdfc9ae7d4612346~40"></div>
//                <span class="activity-item-sender-meta__displayName" title="Mio Test Bot - mioqabot@webex.bot">Mio Test Bot</span>
//                <span class="activity-item-sender-meta__publishedDate">Yesterday, 12:13 PM</span>

                $text = $elm->getText();
                $class = $elm->getAttribute('class');

                // threads?
                if (array_key_exists($lastMessageId, $messages) && Str::contains($class, 'activity-threading-reply'))
                {
                    $messages[$lastMessageId]['threads'][$id] = [
                        'selector' => 'div[id="' . $id . '"]',
                        'id' => $id,
                        'user' => ['id' => '', 'name' => '', 'type' => '', 'avatar' => ''],
                        'text' => $text
                    ];
                }
                else
                {
                    $messages[$id] = [
                                        'selector' => 'div[id="' . $id . '"]',
                                        'id' => $id,
                                        'threads' => [],
                                        'user' => ['id' => '', 'name' => '', 'type' => '', 'avatar' => ''],
                                        'text' => $text];
                    $lastMessageId = $id;
                }
            }
        }

        if ($n)
            $messages = array_splice($messages, -1 * $n);
        $messages = array_reverse($messages);

        return $messages;
    }


    /**
     * *************************************
     *             Messaging
     * *************************************
     */
    public function sendMessageInChat($message)
    {
        $this->waitForElement($this->selectorMessageInput);
        $this->i->click($this->selectorMessageInput);
        $this->i->sendKeys($message);
        $this->i->sendKeys(WebDriverKeys::ENTER);
    }

    public function seeTextInMessageInChat($findByText)
    {
        $this->i->assertIsArray($this->findMessageInChatMessage($findByText));
    }

    public function editMessageInChat($newMessage, $findByText = null, $elm = null)
    {
        throw new \Exception('edit message supported in webex web-chat');
    }

    public function sendReactionChat($reaction, $findByText = null, $elm = null)
    {
        throw new \Exception('sending reaction not supported in webex web-chat');
    }

    public function seeReactionChat($reaction, $findByText = null, $elm = null, $count = null)
    {
        // @todo find by elm..
        $reactions = [];

        if ($findByText)
        {
            $match = $this->findMessageInChatMessage($findByText);
            foreach ((array) Arr::get($match, 'threads') as $thread)
            {
                $text = $thread['text'];
                if (preg_match('/replied with (.+?)/', $text, $matches))
                {
                    $reaction = array_pop($matches);
                    if (!isset($reactions[$reaction]))
                        $reactions[$reaction] = 0;
                    $reactions[$reaction]++;
                }
            }
        }

        $this->i->assertArrayHasKey($reaction, $reactions);
    }

    public function deleteMessageInChat($findByText = null, $elm = null)
    {
        if ($findByText)
        {
            $match = $this->findMessageInChatMessage($findByText);
            $elm = $match['selector'];
        }

        $this->i->moveMouseOver($elm);
        $deleteElm = $elm . ' ' . $this->selectorMessageMenuDelete;
        $this->waitForElement($deleteElm);
        $this->i->click($deleteElm);
        $this->waitForElement($this->selectorMessageMenuDeleteConfirm);
        $this->i->click($this->selectorMessageMenuDeleteConfirm);
        $this->waitForElementNotVisible($this->selectorMessageMenuDeleteConfirm);
    }

    public function threadMessageInChat($threadMessage, $findByText = null, $elm = null)
    {
        if ($findByText)
        {
            $match = $this->findMessageInChatMessage($findByText);
            $elm = $match['selector'];
        }

        $this->i->moveMouseOver($elm);
        $reactionThread = $elm . ' ' . $this->selectorMessageMenuThread;
        $this->waitForElement($reactionThread);
        $this->i->click($reactionThread);

        // wait for thread popup
        $this->waitForElement($this->selectorThreadMessageSend);
        $this->i->click($this->selectorMessageInput);
        $this->i->sendKeys($threadMessage);
        $this->i->click($this->selectorThreadMessageSend);
        $this->waitForElementNotVisible($this->selectorThreadMessageSend);
    }

    public function seeThreadMessageInChat($threadText, $findByText = null, $elm = null)
    {
        $matched = false;

        if ($findByText)
        {
            $match = $this->findMessageInChatMessage($findByText);
            foreach ((array) Arr::get($match, 'threads') as $thread)
            {
                $text = $thread['text'];
                if (Str::contains($text, $threadText))
                {
                    $matched = true;
                    break;
                }
            }
        }

        $this->i->assertEquals(true, $matched);
    }

    protected function findMessageInChatMessage($findByText)
    {
        $match = null;
        $messages = $this->grabLastMessagesSince(0);
        foreach ($messages as $id => $message)
        {
            $text = $message['text'];
            if (Str::contains($text, $findByText)) {
                $match = $message;
                break;
            }
        }

        return $match;
    }

    /**
     * *************************************
     *           Files
     * *************************************
     */
    public function sendFileInChat($files, $message)
    {
        // upload file
        foreach ($files as $file)
        {
            $elm = 'input#messageFileUpload';
            $this->i->attachFile($elm, $file);
            $this->waitForElement('.stagedFiles.is-active');
        }

        $this->i->click($this->selectorMessageInput);
        $this->i->sendKeys($message);

        $this->i->sendKeys(WebDriverKeys::ENTER);

        // wait for upload to finish
        try
        {
            $this->i->waitForElementNotVisible('.shareItem .progressBar', $this->waitForElements * 2);
        } catch (\Exception $e) {}
    }

    public function seeFileInChat($files, $findByText)
    {
        $match = $this->findMessageInChatMessage($findByText);

        $_files = [];
        if ($match && ($elm = $match['selector']))
        {
            $elmsSearch = ['.share .chip-info .chip-text--primary', '.share span.md-content__hover-files--file-name'];
            foreach ($elmsSearch as $elmSearch)
            {
                try
                {
                    $elms = $this->i->findElements($elm . ' ' . $elmSearch);
                    foreach ($elms as $elm)
                    {
                        $name         = $elm->getAttribute('title');
                        $_files[$name] = $name;
                    }
                    $this->i->wait(1);
                } catch (\Exception $exception) {
//                    var_dump($exception->getMessage());
                }
            }
        }

        $matched = 0;
        foreach ($files as $file)
        {
            if (array_key_exists($file, $_files))
                $matched++;
        }

        $this->i->assertCount($matched, $files);
    }


    public  function seeMentionInMessageInChat($text, $mentionUser)
    {
        $this->findMessageInChatMessage($text);
    }

    /**
     * *************************************
     *           Helpers
     * *************************************
     */
    public function waitForAppToLoad()
    {
        $host = $this->i->executeJS('return window.location.host');
        if (!Str::contains($host, 'teams.webex.com'))
            $this->i->amOnUrl($this->workspaceUrl());
        $this->i->wait(2);

        // do we need to login again?
        $elms = $this->i->findElements('.md-input[autocomplete="email"]');
        if (count($elms))
        {
            $this->i->fillField('.md-input[autocomplete="email"]', $this->username);
            $this->waitForElement('form .md-button[alt="Next"]');
            $this->i->click('form .md-button[alt="Next"]');
        }

        // they dont always ask for password
        $elms = $this->i->findElements('#IDToken2');
        if (count($elms))
        {
            $this->i->fillField('#IDToken2', $this->password);
            $this->i->click('#Button1');
        }

        $this->i->waitForElement($this->selectorChatAppLoaded, $this->waitForElements * 2);

        // disable the annonying popup which gets in mouse way
        try {
            $this->waitForElement('button.walkme-custom-balloon-button');
            $this->i->click('button.walkme-custom-balloon-button');
        }catch (\Exception $exception) {}
    }

    protected function workspaceUrl()
    {
        return 'https://' . $this->domain;
    }

    protected function isUserId($name)
    {
        return false;
    }

    protected function isChannelId($name)
    {
        return false;
    }

}
